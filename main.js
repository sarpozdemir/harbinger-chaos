var game = new Phaser.Game(800, 600, Phaser.AUTO, 'gameDiv', {
    preload: preload,
    create: create,
    update: update
});

var spacefield;
var player;
var cursors;
var bullets;
var bulletTime = 0;
var fireButton;
var enemies;
var enemyBullet;
var explosions;
var score = 0;
var scoreText;
var winText;
var firingTimer = 0;
var livingEnemies = [];

function preload() {

    game.load.image('starfield', "assets/bg.png");
    game.load.image('player', "assets/player.png");
    game.load.image('bullet', "assets/bullet.png");
    game.load.image('enemy1', "assets/enemy.png");
    game.load.image('enemyBullet', "assets/enemybullet.png");
    game.load.spritesheet('boom', 'sprites/explosion.png', 32, 32);
}

function create() {
    game.physics.startSystem(Phaser.Physics.ARCADE);

    //scrolling background
    spacefield = game.add.tileSprite(0, 0, 800, 600, 'starfield');

    //our bullets
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(99, 'bullet');
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds, true');

    //enemy bullets
    enemyBullets = game.add.group();
    enemyBullets.enableBody = true;
    enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets.createMultiple(99, 'enemyBullet');
    enemyBullets.setAll('anchor.x', 0.5);
    enemyBullets.setAll('anchor.y', 1);
    enemyBullets.setAll('outOfBoundsKill', true);
    enemyBullets.setAll('checkWorldBounds', true);

    //player
    player = game.add.sprite(400, 500, 'player');
    player.anchor.setTo(0.5, 0.5);
    game.physics.enable(player, Phaser.Physics.ARCADE);

    //enemies
    enemies = game.add.group();
    enemies.enableBody = true;
    enemies.physicsBodyType = Phaser.Physics.ARCADE;

    scoreText = game.add.text(8, 560, 'Score:', {
        font: '32px Arial',
        fill: '#fff'
    });

    winText = game.add.text(game.world.centerX, game.world.centerY, 'You WIN!', {
        font: '32px Arial',
        fill: '#fff'
    });
    winText.visible = false;

    createEnemies();

    //explosion
    explosions = game.add.group();
    explosions.createMultiple(99, 'boom');
    explosions.forEach(setupInvader, this);

    cursors = game.input.keyboard.createCursorKeys();
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

}

function createEnemies() {
    for (var y = 0; y < 4; y++) {
        for (var x = 0; x < 10; x++) {
            var enemy = enemies.create(x * 60, y * 70, 'enemy1');
            enemy.anchor.setTo(0.5, 0.5);
            enemy.body.moves = false;
        }
    }
    enemies.x = 100;
    enemies.y = 50;

    var tween = game.add.tween(enemies).to({
        x: 200
    }, 2000, Phaser.Easing.Linear.NONE, true, 0, 1000, true);

    tween.onRepeat.add(descend, this);
}

function setupInvader(enemies) {

    enemies.anchor.x = 0.5;
    enemies.anchor.y = 0.5;
    enemies.animations.add('boom');

}

function descend() {
    enemies.y += 10;
}

function update() {

    spacefield.tilePosition.y += 2;
    player.body.velocity.setTo(0, 0);
    if (cursors.left.isDown) {
        player.body.velocity.x = -350;
    }

    if (cursors.right.isDown) {
        player.body.velocity.x = 350;
    }

    if (fireButton.isDown) {
        fireBullet();
    }

    scoreText.setText('Score: ' + score);
    if (score == 4000) {

        scoreText.visible = false;
        winText.visible = true;
    }

    if (game.time.now > firingTimer) {
        enemyFires();
    }

    //collisions
    game.physics.arcade.overlap(bullets, enemies, collisionHandler, null, this);
    game.physics.arcade.overlap(enemyBullets, player, harbingerHitsNormandy, null, this);
}



function fireBullet() {
    if (game.time.now > bulletTime) {
        bullet = bullets.getFirstExists(false);
        if (bullet) {
            bullet.reset(player.x, player.y);
            bullet.body.velocity.y = -600;
            bulletTime = game.time.now + 200;
        }
    }
}

function collisionHandler(bullet, enemy) {
    bullet.kill();
    enemy.kill();
    score += 100;
    var explosion = explosions.getFirstExists(false);
    explosion.reset(enemy.x + enemies.x, enemy.y + enemies.y);
    explosion.play('boom', 2000, false, true);
}

function harbingerHitsNormandy(enemybullet, player) {
    enemybullet.kill();
    player.kill();
    this.game.input.keyboard.removeKey(Phaser.Keyboard.SPACEBAR);

    //  player explosion
    var explosion = explosions.getFirstExists(false);
    explosion.reset(player.x, player.y);
    explosion.play('boom', 700, false, true);
    var lostText = game.add.text(game.world.centerX, game.world.centerY, 'You LOST!', {
        font: '32px Arial',
        fill: '#fff'
    });
}

function enemyFires() {
    enemyBullet = enemyBullets.getFirstExists(false);
    livingEnemies.length = 0;
    enemies.forEachAlive(function (enemies) {
        livingEnemies.push(enemies);
    });

    if (enemyBullets && livingEnemies.length > 0) {

        var random = game.rnd.integerInRange(0, livingEnemies.length - 1);
        var shooter = livingEnemies[random];
        //enemy fires
        enemyBullet.reset(shooter.body.x, shooter.body.y);
        game.physics.arcade.moveToObject(enemyBullet, player, 150);
        firingTimer = game.time.now + 2000;
    }
}
